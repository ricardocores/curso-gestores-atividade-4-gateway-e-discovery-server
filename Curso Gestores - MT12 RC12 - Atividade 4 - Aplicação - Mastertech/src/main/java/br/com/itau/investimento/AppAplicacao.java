package br.com.itau.investimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AppAplicacao 
{
    public static void main( String[] args )
    {
        SpringApplication.run(AppAplicacao.class, args);
    }
}
