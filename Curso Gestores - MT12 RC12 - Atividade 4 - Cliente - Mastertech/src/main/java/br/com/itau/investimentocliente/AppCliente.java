package br.com.itau.investimentocliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCliente 
{
    public static void main( String[] args )
    {
        SpringApplication.run(AppCliente.class, args);
    }
}
