package br.com.itau.discoveryserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AppDiscoveryServer {

	public static void main(String[] args) {
		SpringApplication.run(AppDiscoveryServer.class, args);
	}

}


/*
RC: Melhor executar pelo terminal
Dentro da pasta do projeto: ./mvnw spring-boot:run
Executar primeiro este server e depois demais microsserviços. O gateway deve ser o último.

Para acessar Eureka:
http://localhost:9001/

*/